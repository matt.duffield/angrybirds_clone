# easelBox
# http://jeffcole.wordpress.com/2012/02/14/angry-birds-clone-with-easeljs-and-box2dweb/
# https://github.com/jcole/easelBox
# Damage
# http://www.box2d.org/forum/viewtopic.php?f=8&t=6752
# http://blog.sethladd.com/2011/09/box2d-collision-damage-for-javascript.html
# http://blog.sethladd.com/2011/09/box2d-impulse-and-javascript.html
# https://github.com/sethladd/box2d-javascript-fun/blob/master/static/11/bTest.js
# Destroy
# http://www.emanueleferonato.com/2011/10/21/develop-a-flash-game-like-angry-birds-using-box2d-killing-the-pigs/
# http://www.emanueleferonato.com/2011/10/10/develop-a-flash-game-like-angry-birds-using-box2d/
# Catapult example
# http://www.raywenderlich.com/4756/how-to-make-a-catapult-shooting-game-with-cocos2d-and-box2d-part-1
# http://www.raywenderlich.com/4787/how-to-make-a-catapult-shooting-game-with-cocos2d-and-box2d-part-2
# Camera example
# https://gist.github.com/jcole/2155060 
# http://appcodingeasy.com/Gideros-Mobile/Gideros-Camera-Move
# http://www.emanueleferonato.com/2010/04/29/following-a-body-with-the-camera-in-box2d/
# http://www.emanueleferonato.com/2010/05/04/following-a-body-with-the-camera-in-box2d-the-smart-way/



class window.AngryBirdsGame
  # to set up Easel-Box2d world
  PIXELS_PER_METER = 30
  gravityX = 0
  gravityY = 10
  # game-specific
  frameRate = 20
  forceMultiplier = 1.5
  w = 0
  contacts = []

  constructor: (canvas, debugCanvas, statsCanvas) ->    
    @world = new EaselBoxWorld(this, frameRate, canvas, debugCanvas, gravityX, gravityY, PIXELS_PER_METER)    

    contactListener = new Box2D.Dynamics.b2ContactListener

    # contactListener.BeginContact = (contact) ->
    #   console.log('BeginContact: ' + contact.GetFixtureA().GetBody().GetUserData())
      # #Tell both b2dentities about this collision
      # eA = contact.GetFixtureA().GetBody().userData?.b2d_entity
      # eB = contact.GetFixtureB().GetBody().userData?.b2d_entity
      # if eA && eB
      #   eA._onContact(eB) if typeof eA._onContact == 'function'
      #   eB._onContact(eA) if typeof eB._onContact == 'function'
    contactListener.PostSolve = (contact, impulse) ->
      console.log('UserData-A: ' + contact.GetFixtureA().GetBody().GetUserData())
      console.log('UserData-B: ' + contact.GetFixtureB().GetBody().GetUserData())

      isAPiggy = contact.GetFixtureA().GetBody().GetUserData() == 'piggy'
      isBPiggy = contact.GetFixtureB().GetBody().GetUserData() == 'piggy'

      # console.log('UserData-A: ' + contact.GetFixtureA().GetBody().userData)
      if contact.GetFixtureA().GetBody().GetUserData() == 'block' or contact.GetFixtureB().GetBody().GetUserData() == 'piggy'
        count = contact.GetManifold().m_pointCount
        console.log('count: ' + count)  
        maxImpulse = 0.0

        for i in [0..count]
          maxImpulse = Math.max(maxImpulse, impulse.normalImpulses[i])
          # maxImpulse = Box2D.Math.b2Max(maxImpulse, impulse.normalImpulses[i])

        console.log('maxImpulse: ' + maxImpulse)  
        
        if maxImpulse > 1.0
          if isAPiggy
            contacts.push(contact.GetFixtureA().GetBody())
          if isBPiggy
            contacts.push(contact.GetFixtureB().GetBody())



        # normImpulse = impulse.normalImpulses[0]
        # console.log('impulse: ' + normImpulse)
        # if normImpulse < 0.2 
        #   return  #threshold ignore small impacts
        # @head.impulse = normImpulse > 0.6 ? 0.5 : normImpulse
        # console.log('head impulse: ' + @head.impulse)

    @world.box2dWorld.SetContactListener(contactListener)
    

    # optional: set up frame rate display
    @stats = new Stats()
    statsCanvas.appendChild @stats.domElement

    w = canvas.width
    worldWidthPixels = canvas.width
    worldHeightPixels = canvas.height
    @initHeadXPixels = 100
    groundLevelPixels = worldHeightPixels - (37/2)
    
    @world.addImage("/img/sky.jpg", {scaleX: 1.3, scaleY: 1.3})    
    @trees = @world.addImage("/img/trees.png", {scaleX: 0.5, scaleY: 0.5, y: worldHeightPixels - 400 * 0.55})
    @mountains = @world.addImage("/img/mountains.png", {scaleX: 1, scaleY: 1, y: worldHeightPixels - 254 * 1})

    ground = @world.addEntity(
      widthPixels: worldWidthPixels * 2,
      heightPixels: 37,
      # imgSrc: '/img/ground-cropped.png',
      type: 'static',
      friction: 0,
      xPixels: 0, 
      yPixels: groundLevelPixels)   

    @grass = @world.addImage("/img/ground-cropped.png", {scaleX: 2, scaleY: 2, y: groundLevelPixels - 37})

    # draw part of the slingshot
    # @world.addImage("/img/catapult_50x150.png", {x: @initHeadXPixels - 30, y:  worldHeightPixels - 160})
    @world.addImage("/img/slingshot_a.png", {x: @initHeadXPixels, y:  worldHeightPixels - 160})

    # setup head
    @head = @world.addEntity(
      radiusPixels: 17,
      imgSrc: '/img/angry_birds_sprite.png',
      frames: {width:35, height:35},#, regX:35, regY:35},
      # startFrame: 1, 
      animations: {stand: [0,0,'stand']},
      startAnimation: 'stand', 
      type: 'static',
      density: 2,
      friction: 1,
      bullet: true,
      xPixels: @initHeadXPixels, 
      yPixels: groundLevelPixels - 140) 

    @head.body.SetUserData('bird')
    @head.selected = false
    @head.easelObj.onPress = (eventPress) =>
      @head.selected = true
      @head.initPositionXpixels = eventPress.stageX
      @head.initPositionYpixels = eventPress.stageY
      
      eventPress.onMouseMove = (event) =>
        @head.setState(xPixels: event.stageX, yPixels: event.stageY)
    
      eventPress.onMouseUp = (event) =>
        @head.selected = false
        @head.setType "dynamic"  
        forceX = (@head.initPositionXpixels - event.stageX) * forceMultiplier
        forceY = (@head.initPositionYpixels - event.stageY) * forceMultiplier
        console.log('forceX: ' + forceX)
        console.log('forceY: ' + forceY)
        @head.body.ApplyImpulse(
          @world.vector(forceX, forceY),
          @world.vector(@head.body.GetPosition().x, @head.body.GetPosition().y)
        )    


    # draw other part of slingshot
    @world.addImage("/img/slingshot2_a.png", {x: @initHeadXPixels - 20, y:  worldHeightPixels - 154})


    # draw pyramid    
    blockWidth = 15 
    blockHeight = 60 
    leftPyamid = 600
    levels = 3
    topOfPyramid = groundLevelPixels - levels *  (blockHeight + blockWidth) + 26
    for i in [0...levels]
      for j in [0..i+1]
          x =  leftPyamid + (j-i/2) * blockHeight 
          y = topOfPyramid + i * (blockHeight + blockWidth)
          myBlock =  @world.addEntity(
            widthPixels: blockWidth,
            heightPixels: blockHeight,
            imgSrc: '/img/block1_15x60_2.png',
            xPixels: x, 
            yPixels: y)
          myBlock.body.SetUserData('block')

          if j <= i
            myBlock = @world.addEntity(
              widthPixels: blockWidth,
              heightPixels: blockHeight,
              imgSrc: '/img/block1_15x60_2.png',             
              xPixels: x + blockHeight / 2,
              yPixels: y - (blockHeight + blockWidth) / 2
              angleDegrees: 90)
            myBlock.body.SetUserData('block')

            ghost = @world.addEntity(
              widthPixels: 30,
              heightPixels: 36,
              imgSrc: '/img/badpiggy_30x36.png',
              xPixels: x + (blockHeight / 2),
              yPixels: y + 11)
            myBlock.body.SetUserData('piggy')


  # optional: a callback for each EaselBox2dWorld tick()
  tick: () ->
    @stats.update()
    console.log('Contacts: ' + contacts.length)

    # if (@head.body.GetLinearVelocity().x > 2)
    #   @trees.x = (@trees.x - 0.8)
    #   if (@trees.x + 838 <= 0) 
    #     @trees.x = w
    #   @mountains.x = (@mountains.x - 0.5)
    #   if (@mountains.x + 838 <= 0) 
    #     @mountains.x = w
    #   @grass.x = (@grass.x - 0.8)
    #   if (@grass.x + 838 <= 0) 
    #     @grass.x = w
    # if (@head.body.GetLinearVelocity().x < -2)
    #   @trees.x = (@trees.x + 0.8)
    #   if (@trees.x + 838 <= 0) 
    #     @trees.x = w
    #   @mountains.x = (@mountains.x + 0.5)
    #   if (@mountains.x + 838 <= 0) 
    #     @mountains.x = w
    #   @grass.x = (@grass.x + 0.8)
    #   if (@grass.x + 838 <= 0) 
    #     @grass.x = w
            

    headTravel = @head.easelObj.x - @initHeadXPixels
    # console.log('head: ' + @head.easelObj.x)
    # console.log('initHeadXPixels: ' + @initHeadXPixels)
    # console.log('headTravel: ' + headTravel)
    if headTravel > 40 and headTravel < w
      @world.easelStage.x = 0 - headTravel
      # console.log('World X: ' + @world.easelStage.x)
                  